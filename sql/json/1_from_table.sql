create table if not exists trip (
    id int unsigned auto_increment primary key
    , name varchar(255)
    , created_at datetime
)
;

insert into trip (
    name
    , created_at
)
values
    (
        'belledone'
        , sysdate()
    )
    , (
        'chartreuse'
        , sysdate()
    )
;

select json_object('name', name, 'created_at', created_at)
from trip
;

select json_objectagg(name, created_at)
from trip
;

select json_arrayagg(
    json_object('name', name, 'created_at', created_at)
)
from trip
;

-- ERROR 1111 (HY000) at line 37: Invalid use of group function
-- select json_arrayagg(
--     json_objectagg(name, created_at)
-- )
-- from trip
-- ;
