select
    some_values.banana as fruit
from (
    values
        ('banana')
        , ('orange')
        , ('peach')
) as some_values
