create table mydb.fruit (
    id int primary key,
    name varchar(255)
)
;

insert into mydb.fruit (id, name)
values
    (1, 'apple')
    , (2, 'banana')
    , (3, 'pear')
;

select
    *
    , 'initial state' as current_state
from mydb.fruit
;

insert into mydb.fruit (id, name)
values
    (1, 'peach')
    , (2, 'cherry')
    , (3, 'lemon')
on duplicate key update
    name = values(name) -- use the new value
;

select
    *
    , 'use new value [name = values(name)]' as current_state
from mydb.fruit
;

insert into mydb.fruit (id, name)
values
    (1, 'peach')
    , (2, 'cherry')
    , (3, 'lemon')
on duplicate key update
    name = name -- keep the existing value
;

select
    *
    , 'keep existing value [name = name]' as current_state
from mydb.fruit
;
