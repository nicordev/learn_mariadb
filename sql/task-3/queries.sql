-- code goes here 
select
  o.Order_id
  , m.Discount
  , o.Order_Date
  , lead(
    m.Discount
  ) over (
    order by o.Order_Date
  ) as LeadDiscount
  , lag(
    m.Discount
  ) over (
    order by o.Order_Date
  ) as LagDiscount
from orders o
inner join market m on m.Order_id = o.Order_id
order by o.Order_Date
