select
    current_date() as today
    , date_add(current_date(), interval 8 week) as future
    , date_sub(current_date(), interval 8 week) as past
;
