create table fruit (
    id int auto_increment primary key
    , basket_id int
    , `name` varchar(100)

    , constraint fk_basket
        foreign key (basket_id) references basket(id)
)
;

create table basket (
    id int auto_increment primary key
    , `name` varchar(100)
    , `quantity` int
    , check (quantity > 0)
)
;

insert into basket (
    `name`
    , `quantity`
)
values 
    ('panier varié', 0)
    , ('panier d''oranges', 0)
;

insert into fruit (
    basket_id
    , `name`
) 
values 
    (1, 'banana')
    , (1, 'banana')
    , (1, 'orange')
    , (1, 'lemon')
    , (1, 'pear')
    , (1, 'pear')
    , (2, 'orange')
    , (2, 'orange')
    , (2, 'orange')
;
