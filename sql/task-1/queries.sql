-- code goes here 
select 
  p.Product_Category
  , m.Sales
  , rank() over (partition by p.Product_Category order by m.Sales desc) as SalesRank
from product p
inner join market m on m.Product_id = p.Product_id
order by p.Product_Category, SalesRank asc
