DOCKER_COMPOSE = docker compose --file=docker/compose.yaml
EXEC_DATABASE = $(DOCKER_COMPOSE) exec mariadb
TASK ?= 1

.PHONY: start
start: ## make start
	$(DOCKER_COMPOSE) up --detach

.PHONY: down
down: ## make down
	$(DOCKER_COMPOSE) down --volumes --remove-orphans

.PHONY: reset
reset: down start

.PHONY: bash
bash:
	$(EXEC_DATABASE) bash

.PHONY: connect
connect:
	$(EXEC_DATABASE) mariadb --user dev

.PHONY: adminer
adminer:
	@echo http://127.0.0.1:8087/login

.PHONY: copy-sql
copy-sql:
	@$(DOCKER_COMPOSE) cp sql mariadb:/

.PHONY: task
task: ## make task TASK=2
task: copy-sql
	@$(EXEC_DATABASE) bash -c 'mariadb --user dev --database mydb < /sql/task-${TASK}/init.sql'
	@$(EXEC_DATABASE) bash -c 'mariadb --user dev --database mydb < /sql/task-${TASK}/queries.sql'
	@$(MAKE) drop

.PHONY: execute
execute: ## make execute FILE=variable/variable.sql
execute: copy-sql
	@$(EXEC_DATABASE) bash -c 'mariadb --user dev --database mydb < /sql/${FILE}'
	@$(MAKE) drop

.PHONY: execute
execute-dir: ## make execute-dir DIR=json
execute-dir: copy-sql
	for file in sql/$(DIR)/*; do $(EXEC_DATABASE) bash -c 'mariadb --user dev --database mydb < '$$file; done
	@$(MAKE) drop

.PHONY: drop
drop: copy-sql
	@$(EXEC_DATABASE) bash -c 'mariadb --user dev --database mydb < /sql/drop.sql'

.DEFAULT_GOAL = help
help:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/' 
