#! /usr/bin/env php
<?php

declare(strict_types=1);

define('SCRIPT_NAME', pathinfo(array_shift($argv), PATHINFO_BASENAME));

class command
{
    public static function execute(array $arguments): void
    {
        if (count($arguments) < 1) {
            echo sprintf(
                <<< DESCRIPTION
                %s %s\033[33m sql_query\033[0m
                \033[0m
                DESCRIPTION,
                SCRIPT_NAME,
                __FUNCTION__,
            );

            return;
        }

        try {
            $sql = implode(' ', $arguments);
            $pdo = self::connect();

            echo "$sql\n";
        } catch (\Throwable $error) {
            self::write_error($error->getMessage());
            exit(1);
        }
    }

    public static function list_actions(): array
    {
        $class = new ReflectionClass(self::class);
        $methods = array_map(
            fn (ReflectionMethod $method) => $method->getName(),
            $class->getMethods(ReflectionMethod::IS_PUBLIC),
        );

        $actions = array_filter(
            $methods,
            fn (string $method) => $method !== 'list_actions',
        );

        sort($actions);

        return $actions;
    }

    private static function connect(): PDO
    {
        $dsn = 'mysql:';
        $user = 'dev';
        $password = '';

        return new PDO($dsn, $user, $password); // TODO: add pdo_mysql module
    }

    private static function parse_argument(array $arguments, ?string $long_name = null, ?string $short_name = null): ?array
    {
        if (strpos($long_name, '--') === 0) {
            $long_name = substr($long_name, 2);
        }

        if (strpos($short_name, '-') === 0) {
            $short_name = substr($short_name, 1);
        }

        $values = [];

        foreach ($arguments as $key => $argument) {
            $matches = [];

            $long_name_found = ($long_name !== null) && preg_match(
                <<< REGEX
                #^--{$long_name}=?(.*)#
                REGEX,
                $argument,
                $matches
            );

            if ($long_name_found) {
                $match = $matches[1] ?? '';

                $values[] = $match === ''
                    ? $arguments[$key + 1]
                    : $match
                ;
                continue;
            }

            $short_name_found = ($short_name !== null) && (strpos($argument, "-{$short_name}") === 0);

            if ($short_name_found) {
                $values[] = $arguments[$key + 1];
            }
        }

        return $values === []
            ? null
            : $values
        ;
    }

    private static function write_error(string $message): void
    {
        fwrite(STDERR, $message."\n");
    }
}

$action = array_shift($argv);

if ($argc <= 1) {
    echo implode("\n", command::list_actions())."\n";
    exit;
}

$actions = command::list_actions();

if (!in_array($action, $actions)) {
    echo "Unknown action [$action].\n";
    exit;
}

command::$action($argv);

