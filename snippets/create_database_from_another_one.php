<?php

function create_database_from_another_one(PDO $database, string $source_database, string $new_database, array $tables): void
{
    $sql = <<< SQL
        create or replace database {$new_database}
    SQL;

    $database->exec($sql);

    foreach ($tables as $table) {
        $sql = <<< SQL
            create table {$new_database}.{$table} like {$source_database}.{$table}
        SQL;

        $database->exec($sql);

        $sql = <<< SQL
            insert into {$new_database}.{$table}
            select * from {$source_database}.{$table}
            limit 10
        SQL;

        $database->exec($sql);
    }
}
