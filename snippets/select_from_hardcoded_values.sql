select
    skier.first_name
    , skier.birthday
    , skier.age
from (
    select 
        'bob' as first_name
        , '1987-04-03' as birthday
        , null as age
    union all
    select
        'lara'
        , null
        , 6
) skier
;
