<?php

declare(strict_types=1);

final class NextValueDemo
{
    public function init(): void
    {
        $sql = <<< SQL
            create database or replace orchard
        SQL;

        $this->query($sql);

        $sql = <<< SQL
            create table fruit (
                id int auto_increment primary key
                , basket_id int
                , `name` varchar(100)

                , constraint fk_basket
                    foreign key (basket_id) references basket(id)
            )
        SQL;

        $this->query($sql);

        $sql = <<< SQL
            create table basket (
                id int auto_increment primary key
                , name varchar(255) not null
            )
        SQL;

        $this->query($sql);
    }

    public function create_basket_with_fruit(array $data)
    {
        $sql = <<< SQL
            select next_value(sequence_name_here)
        SQL;

        $new_id = $this->query($sql);

        $data['basket']['id'] = $new_id;
        $data['fruit']['basket_id'] = $new_id;

        $sql = <<< SQL
            insert into fruit (id, name, basket_id)
            values (:id, :name, :basket_id)
        SQL;

        $this->query($sql, $data['fruit']);

        $sql = <<< SQL
            insert into basket (id, name)
            values (:id, :name)
        SQL;

        $this->query($sql, $data['basket']);
    }

    private function query(string $sql, array $parameters = [])
    {
        // WIP
    }
}
