with past_orders as (
    select order_id
    from order
    where order.created_at < sysdate()
)
select item_id
from item
where item.order_id in (select order_id from past_orders)
;
