<?php

function insert(PDO $database, array $fruits): void
{
    $sql = <<< 'SQL'
        insert into orchard.fruit (min, price, ratio)
        values
    SQL;
    $values = [];

    foreach ($fruits as $fruit) {
        $values[] = sprintf(
            "(%d, %f, %f)",
            $fruit->min(),
            $fruit->price(),
            $fruit->ratio(),
        );
    }

    if ([] === $values) {
        return;
    }

    $sql .= implode(',', $values);

    $database->exec($sql);
}
