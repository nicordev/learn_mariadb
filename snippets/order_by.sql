-- https://dba.stackexchange.com/questions/207581/order-by-numbers-followed-by-alphabet-and-then-null-values

select
    *
from
    t
order by
    case
        when col regexp '^[0-9]' then 1
        when col regexp '^[a-zA-Z]' then 2
        when col = '' or col is null then 3
    end
    , col * 1 /*this converts to number, so that 100 is not sorted before 2*/
    , col /*finally sort strings correctly*/
