# mariadb

## Use with docker compose

```sh
make start
```

## Use with docker

> [blog - docker](https://mariadb.com/resources/blog/get-started-with-mariadb-using-docker-in-3-steps/)

create a container:

```sh
docker run -p 127.0.0.1:3306:3306 --name my-mariadb-container -e MARIADB_ROOT_PASSWORD=MyUnbreakablePassword -d mariadb:latest
```

connect to the container:

```sh
docker exec -it my-mariadb-container mariadb --user root -pMyUnbreakablePassword
```

## Use a database

list databases:

```sh
show databases;
```

select a database:

```sh
use my_database_name_here;
```

## Training

- [market](https://onecompiler.com/challenges/3zs6w3938/sql-foundation-w4d2-ct-ds3a)
